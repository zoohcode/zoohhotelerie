-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 22 jan. 2021 à 16:54
-- Version du serveur :  5.7.24
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_hotelerie`
--

-- --------------------------------------------------------

--
-- Structure de la table `detailfacture`
--

CREATE TABLE `detailfacture` (
  `id` int(11) NOT NULL,
  `factures_id` int(11) DEFAULT NULL,
  `montant` int(11) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201214191540', '2020-12-14 19:15:55', 9732),
('DoctrineMigrations\\Version20201215073613', '2020-12-15 07:36:33', 2160),
('DoctrineMigrations\\Version20201230155456', '2020-12-30 15:55:13', 3481),
('DoctrineMigrations\\Version20210104094805', '2021-01-04 09:48:18', 1638),
('DoctrineMigrations\\Version20210104101933', '2021-01-04 10:19:46', 1502),
('DoctrineMigrations\\Version20210104194813', '2021-01-04 19:48:33', 991),
('DoctrineMigrations\\Version20210104195302', '2021-01-04 19:53:14', 871),
('DoctrineMigrations\\Version20210104201955', '2021-01-04 20:20:12', 580),
('DoctrineMigrations\\Version20210109100820', '2021-01-09 10:08:32', 1705),
('DoctrineMigrations\\Version20210110081300', '2021-01-10 08:13:13', 915),
('DoctrineMigrations\\Version20210110090135', '2021-01-10 09:01:48', 691),
('DoctrineMigrations\\Version20210113203211', '2021-01-13 20:32:28', 1463),
('DoctrineMigrations\\Version20210113203518', '2021-01-13 20:35:34', 742),
('DoctrineMigrations\\Version20210118165252', '2021-01-18 16:53:05', 1919);

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE `facture` (
  `id` int(11) NOT NULL,
  `clients_id` int(11) DEFAULT NULL,
  `numfacture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datefacture` date DEFAULT NULL,
  `montantttc` int(11) DEFAULT NULL,
  `montantht` int(11) DEFAULT NULL,
  `montantnet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `occupant`
--

CREATE TABLE `occupant` (
  `id` int(11) NOT NULL,
  `clients_id` int(11) DEFAULT NULL,
  `chambres_id` int(11) DEFAULT NULL,
  `debut` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  `nbrnuite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `occupant`
--

INSERT INTO `occupant` (`id`, `clients_id`, `chambres_id`, `debut`, `fin`, `nbrnuite`) VALUES
(4, 49, 227, '2021-01-13 00:00:00', '2021-01-14 00:00:00', NULL),
(5, 51, 229, '2021-01-13 00:00:00', '2021-01-15 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `paiement`
--

CREATE TABLE `paiement` (
  `id` int(11) NOT NULL,
  `modepaiements_id` int(11) DEFAULT NULL,
  `clients_id` int(11) DEFAULT NULL,
  `datapaiement` date DEFAULT NULL,
  `montanttotal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pmodepaiement`
--

CREATE TABLE `pmodepaiement` (
  `id` int(11) NOT NULL,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statut` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `poption`
--

CREATE TABLE `poption` (
  `id` int(11) NOT NULL,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `poption`
--

INSERT INTO `poption` (`id`, `intitule`) VALUES
(73, 'Climatiseur'),
(74, 'Jacouzi'),
(75, 'Brasseur'),
(76, 'Mini-bar');

-- --------------------------------------------------------

--
-- Structure de la table `preduction`
--

CREATE TABLE `preduction` (
  `id` int(11) NOT NULL,
  `tchambres_id` int(11) DEFAULT NULL,
  `tclients_id` int(11) DEFAULT NULL,
  `datereduct` date DEFAULT NULL,
  `montantreduuct` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `psociete`
--

CREATE TABLE `psociete` (
  `id` int(11) NOT NULL,
  `ptypesocietes_id` int(11) DEFAULT NULL,
  `nomsocite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifu` int(11) DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mcf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rccm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ptypesociete`
--

CREATE TABLE `ptypesociete` (
  `id` int(11) NOT NULL,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tcategorie`
--

CREATE TABLE `tcategorie` (
  `id` int(11) NOT NULL,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_supprimer` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tcategorie`
--

INSERT INTO `tcategorie` (`id`, `intitule`, `code`, `is_supprimer`) VALUES
(101, 'Luxe', NULL, 0),
(102, 'Ordinaire', NULL, 0),
(103, 'Présidentielle', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tchambre`
--

CREATE TABLE `tchambre` (
  `id` int(11) NOT NULL,
  `tcategories_id` int(11) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `numchambre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isoccuper` tinyint(1) NOT NULL,
  `is_supprimer` tinyint(1) NOT NULL,
  `is_reserver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tchambre`
--

INSERT INTO `tchambre` (`id`, `tcategories_id`, `prix`, `description`, `numchambre`, `isoccuper`, `is_supprimer`, `is_reserver`) VALUES
(227, 101, 19567181, NULL, '0', 1, 0, 0),
(228, 101, 11864317, NULL, '1', 0, 0, 0),
(229, 101, 15101803, NULL, '2', 1, 0, 0),
(230, 101, 17223158, NULL, '3', 0, 0, 0),
(231, 101, 17195424, NULL, '4', 0, 0, 0),
(232, 101, 11302580, NULL, '5', 0, 0, 0),
(233, 101, 19014948, NULL, '6', 0, 0, 0),
(234, 101, 16007063, NULL, '7', 0, 0, 0),
(235, 101, 15549977, NULL, '8', 0, 0, 0),
(236, 101, 16730334, NULL, '9', 0, 0, 0),
(237, 101, 12188380, NULL, '10', 0, 0, 0),
(238, 102, 12494563, NULL, '11', 0, 0, 0),
(239, 102, 19514466, NULL, '12', 0, 0, 0),
(240, 102, 17851794, NULL, '13', 0, 0, 0),
(241, 102, 15853243, NULL, '14', 0, 0, 0),
(242, 102, 15752030, NULL, '15', 0, 0, 0),
(243, 102, 11030855, NULL, '16', 0, 0, 0),
(244, 102, 13111564, NULL, '17', 0, 0, 0),
(245, 102, 14535688, NULL, '18', 0, 0, 0),
(246, 102, 18080724, NULL, '19', 0, 0, 0),
(247, 102, 19443595, NULL, '20', 0, 0, 0),
(248, 103, 13088158, NULL, '21', 0, 0, 0),
(249, 103, 13779240, NULL, '22', 0, 0, 0),
(250, 103, 11092806, NULL, '23', 0, 0, 0),
(251, 103, 17009145, NULL, '24', 0, 0, 0),
(252, 103, 14823691, NULL, '25', 0, 0, 0),
(253, 103, 16654818, NULL, '26', 0, 0, 0),
(254, 103, 18553647, NULL, '27', 0, 0, 0),
(255, 103, 18363510, NULL, '28', 0, 0, 0),
(256, 103, 10464904, NULL, '29', 0, 0, 0),
(257, 103, 17011095, NULL, '30', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tchambre_poption`
--

CREATE TABLE `tchambre_poption` (
  `tchambre_id` int(11) NOT NULL,
  `poption_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tchambre_poption`
--

INSERT INTO `tchambre_poption` (`tchambre_id`, `poption_id`) VALUES
(227, 73),
(227, 74),
(227, 75),
(227, 76),
(228, 73),
(228, 74),
(228, 75),
(228, 76),
(229, 73),
(229, 74),
(229, 75),
(229, 76),
(230, 73),
(230, 74),
(230, 75),
(230, 76),
(231, 73),
(231, 74),
(231, 75),
(231, 76),
(232, 73),
(232, 74),
(232, 75),
(232, 76),
(233, 73),
(233, 74),
(233, 75),
(233, 76),
(234, 73),
(234, 74),
(234, 75),
(234, 76),
(235, 73),
(235, 74),
(235, 75),
(235, 76),
(236, 73),
(236, 74),
(236, 75),
(236, 76),
(237, 73),
(237, 74),
(237, 75),
(237, 76),
(238, 73),
(238, 74),
(238, 75),
(239, 73),
(239, 74),
(239, 75),
(240, 73),
(240, 74),
(240, 75),
(241, 73),
(241, 74),
(241, 75),
(242, 73),
(242, 74),
(242, 75),
(243, 73),
(243, 74),
(243, 75),
(244, 73),
(244, 74),
(244, 75),
(245, 73),
(245, 74),
(245, 75),
(246, 73),
(246, 74),
(246, 75),
(247, 73),
(247, 74),
(247, 75),
(248, 73),
(248, 74),
(249, 73),
(249, 74),
(250, 73),
(250, 74),
(251, 73),
(251, 74),
(252, 73),
(252, 74),
(253, 73),
(253, 74),
(254, 73),
(254, 74),
(255, 73),
(255, 74),
(256, 73),
(256, 74),
(257, 73),
(257, 74);

-- --------------------------------------------------------

--
-- Structure de la table `tclient`
--

CREATE TABLE `tclient` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numpasseport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datexp` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tclient`
--

INSERT INTO `tclient` (`id`, `nom`, `prenom`, `telephone`, `numpasseport`, `datexp`) VALUES
(49, 'Duval', 'Éléonore', NULL, NULL, NULL),
(50, 'Marie', 'Agathe', NULL, NULL, NULL),
(51, 'Petit', 'Thérèse', NULL, NULL, NULL),
(52, 'Le Gall', 'Thierry', NULL, NULL, NULL),
(53, 'Michel', 'Élodie', NULL, NULL, NULL),
(54, 'Guillon', 'William', NULL, NULL, NULL),
(55, 'Blot', 'Virginie', NULL, NULL, NULL),
(56, 'Guyon', 'Luce', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tprestation`
--

CREATE TABLE `tprestation` (
  `id` int(11) NOT NULL,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `codeprestation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isvendable` tinyint(1) NOT NULL,
  `isfree` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tprestation`
--

INSERT INTO `tprestation` (`id`, `intitule`, `prix`, `codeprestation`, `isvendable`, `isfree`) VALUES
(7, 'piscine', 3000, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `treservation`
--

CREATE TABLE `treservation` (
  `id` int(11) NOT NULL,
  `tclients_id` int(11) DEFAULT NULL,
  `datedebut` date DEFAULT NULL,
  `datefin` date DEFAULT NULL,
  `montantarh` int(11) DEFAULT NULL,
  `statut` tinyint(1) NOT NULL,
  `chambres_id` int(11) DEFAULT NULL,
  `is_confirmer` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `treservation`
--

INSERT INTO `treservation` (`id`, `tclients_id`, `datedebut`, `datefin`, `montantarh`, `statut`, `chambres_id`, `is_confirmer`) VALUES
(7, 51, '2021-01-13', '2021-01-15', 10000, 0, NULL, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `detailfacture`
--
ALTER TABLE `detailfacture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_157F9A08E9D518F9` (`factures_id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `facture`
--
ALTER TABLE `facture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FE866410AB014612` (`clients_id`);

--
-- Index pour la table `occupant`
--
ALTER TABLE `occupant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E7D9DBCAAB014612` (`clients_id`),
  ADD KEY `IDX_E7D9DBCA8BEC3FB7` (`chambres_id`);

--
-- Index pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B1DC7A1E8EFBA58A` (`modepaiements_id`),
  ADD KEY `IDX_B1DC7A1EAB014612` (`clients_id`);

--
-- Index pour la table `pmodepaiement`
--
ALTER TABLE `pmodepaiement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `poption`
--
ALTER TABLE `poption`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `preduction`
--
ALTER TABLE `preduction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_422DAD28BCF2F2BF` (`tchambres_id`),
  ADD KEY `IDX_422DAD286DF7BEF9` (`tclients_id`);

--
-- Index pour la table `psociete`
--
ALTER TABLE `psociete`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4D7BAC3AE4C2C61C` (`ptypesocietes_id`);

--
-- Index pour la table `ptypesociete`
--
ALTER TABLE `ptypesociete`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tcategorie`
--
ALTER TABLE `tcategorie`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B35C8F0E77153098` (`code`);

--
-- Index pour la table `tchambre`
--
ALTER TABLE `tchambre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_155D7B82851B54C4` (`numchambre`),
  ADD KEY `IDX_155D7B828F1E17AC` (`tcategories_id`);

--
-- Index pour la table `tchambre_poption`
--
ALTER TABLE `tchambre_poption`
  ADD PRIMARY KEY (`tchambre_id`,`poption_id`),
  ADD KEY `IDX_9694A965DE187BF` (`tchambre_id`),
  ADD KEY `IDX_9694A96540EE42F` (`poption_id`);

--
-- Index pour la table `tclient`
--
ALTER TABLE `tclient`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tprestation`
--
ALTER TABLE `tprestation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `treservation`
--
ALTER TABLE `treservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_75D6845D6DF7BEF9` (`tclients_id`),
  ADD KEY `IDX_75D6845D8BEC3FB7` (`chambres_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `detailfacture`
--
ALTER TABLE `detailfacture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `facture`
--
ALTER TABLE `facture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `occupant`
--
ALTER TABLE `occupant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `paiement`
--
ALTER TABLE `paiement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pmodepaiement`
--
ALTER TABLE `pmodepaiement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `poption`
--
ALTER TABLE `poption`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT pour la table `preduction`
--
ALTER TABLE `preduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `psociete`
--
ALTER TABLE `psociete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ptypesociete`
--
ALTER TABLE `ptypesociete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tcategorie`
--
ALTER TABLE `tcategorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT pour la table `tchambre`
--
ALTER TABLE `tchambre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT pour la table `tclient`
--
ALTER TABLE `tclient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT pour la table `tprestation`
--
ALTER TABLE `tprestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `treservation`
--
ALTER TABLE `treservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `detailfacture`
--
ALTER TABLE `detailfacture`
  ADD CONSTRAINT `FK_157F9A08E9D518F9` FOREIGN KEY (`factures_id`) REFERENCES `facture` (`id`);

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `FK_FE866410AB014612` FOREIGN KEY (`clients_id`) REFERENCES `tclient` (`id`);

--
-- Contraintes pour la table `occupant`
--
ALTER TABLE `occupant`
  ADD CONSTRAINT `FK_E7D9DBCA8BEC3FB7` FOREIGN KEY (`chambres_id`) REFERENCES `tchambre` (`id`),
  ADD CONSTRAINT `FK_E7D9DBCAAB014612` FOREIGN KEY (`clients_id`) REFERENCES `tclient` (`id`);

--
-- Contraintes pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD CONSTRAINT `FK_B1DC7A1E8EFBA58A` FOREIGN KEY (`modepaiements_id`) REFERENCES `pmodepaiement` (`id`),
  ADD CONSTRAINT `FK_B1DC7A1EAB014612` FOREIGN KEY (`clients_id`) REFERENCES `tclient` (`id`);

--
-- Contraintes pour la table `preduction`
--
ALTER TABLE `preduction`
  ADD CONSTRAINT `FK_422DAD286DF7BEF9` FOREIGN KEY (`tclients_id`) REFERENCES `tclient` (`id`),
  ADD CONSTRAINT `FK_422DAD28BCF2F2BF` FOREIGN KEY (`tchambres_id`) REFERENCES `tchambre` (`id`);

--
-- Contraintes pour la table `psociete`
--
ALTER TABLE `psociete`
  ADD CONSTRAINT `FK_4D7BAC3AE4C2C61C` FOREIGN KEY (`ptypesocietes_id`) REFERENCES `ptypesociete` (`id`);

--
-- Contraintes pour la table `tchambre`
--
ALTER TABLE `tchambre`
  ADD CONSTRAINT `FK_155D7B828F1E17AC` FOREIGN KEY (`tcategories_id`) REFERENCES `tcategorie` (`id`);

--
-- Contraintes pour la table `tchambre_poption`
--
ALTER TABLE `tchambre_poption`
  ADD CONSTRAINT `FK_9694A96540EE42F` FOREIGN KEY (`poption_id`) REFERENCES `poption` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_9694A965DE187BF` FOREIGN KEY (`tchambre_id`) REFERENCES `tchambre` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `treservation`
--
ALTER TABLE `treservation`
  ADD CONSTRAINT `FK_75D6845D6DF7BEF9` FOREIGN KEY (`tclients_id`) REFERENCES `tclient` (`id`),
  ADD CONSTRAINT `FK_75D6845D8BEC3FB7` FOREIGN KEY (`chambres_id`) REFERENCES `tchambre` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
