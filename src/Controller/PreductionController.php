<?php

namespace App\Controller;

use App\Entity\Preduction;
use App\Form\PreductionType;
use App\Repository\PreductionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/preduction")
 */
class PreductionController extends AbstractController
{
    /**
     * @Route("/", name="preduction_index", methods={"GET"})
     */
    public function index(PreductionRepository $preductionRepository): Response
    {
        return $this->render('preduction/index.html.twig', [
            'preductions' => $preductionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="preduction_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $preduction = new Preduction();
        $form = $this->createForm(PreductionType::class, $preduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($preduction);
            $entityManager->flush();

            return $this->redirectToRoute('preduction_index');
        }

        return $this->render('preduction/new.html.twig', [
            'preduction' => $preduction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="preduction_show", methods={"GET"})
     */
    public function show(Preduction $preduction): Response
    {
        return $this->render('preduction/show.html.twig', [
            'preduction' => $preduction,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="preduction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Preduction $preduction): Response
    {
        $form = $this->createForm(PreductionType::class, $preduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('preduction_index');
        }

        return $this->render('preduction/edit.html.twig', [
            'preduction' => $preduction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="preduction_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Preduction $preduction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$preduction->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($preduction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('preduction_index');
    }
}
