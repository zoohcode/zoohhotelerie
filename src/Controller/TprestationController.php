<?php

namespace App\Controller;

use App\Entity\Tprestation;
use App\Form\TprestationType;
use App\Repository\TprestationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/prestation")
 */
class TprestationController extends AbstractController
{
    /**
     * @Route("/", name="tprestation_index",  methods={"GET","POST"})
     */
    public function index(TprestationRepository $tprestationRepository, Request $request): Response
    {
        $prestation = new Tprestation();
        $valider = $request->request->get('save');
        if (isset($valider)) {
            $prestation->setIntitule($request->request->get('intitule'));
            $prestation->setPrix($request->request->get('prix'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($prestation);
            $entityManager->flush();
        }
        return $this->render('tprestation/index.html.twig', [
            'prestation' => $prestation,
            'prestations' => $tprestationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tprestation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tprestation = new Tprestation();
        $form = $this->createForm(TprestationType::class, $tprestation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tprestation);
            $entityManager->flush();

            return $this->redirectToRoute('tprestation_index');
        }

        return $this->render('tprestation/new.html.twig', [
            'tprestation' => $tprestation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tprestation_show", methods={"GET"})
     */
    public function show(Tprestation $tprestation): Response
    {
        return $this->render('tprestation/show.html.twig', [
            'tprestation' => $tprestation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tprestation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tprestation $tprestation): Response
    {
        $form = $this->createForm(TprestationType::class, $tprestation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tprestation_index');
        }

        return $this->render('tprestation/edit.html.twig', [
            'tprestation' => $tprestation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tprestation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tprestation $tprestation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tprestation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tprestation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tprestation_index');
    }
}
