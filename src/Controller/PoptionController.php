<?php

namespace App\Controller;

use App\Entity\Poption;
use App\Form\PoptionType;
use App\Repository\PoptionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/option")
 */
class PoptionController extends AbstractController
{
    /**
     * @Route("/", name="poption_index", methods={"GET","POST"})
     */
    public function index(PoptionRepository $poptionRepository, ValidatorInterface $validator, Request $request): Response
    {
        $poption = new Poption();
        $errors = null;
        $valider = $request->request->get('save');
        if (isset($valider)) {
            $poption->setIntitule($request->request->get('intitule'));
            $errors = $validator->validate($poption);
    
            if (count($errors) == 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($poption);
                $entityManager->flush();
            }
        }
    
        return $this->render('poption/index.html.twig', [
            'poptions' => $poptionRepository->findAll(),
            'errors' => $errors,
            'option' =>  $poption
        ]);
    }

    /**
     * @Route("/new", name="poption_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $poption = new Poption();
        $form = $this->createForm(PoptionType::class, $poption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($poption);
            $entityManager->flush();

            return $this->redirectToRoute('poption_index');
        }

        return $this->render('poption/new.html.twig', [
            'poption' => $poption,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="poption_show", methods={"GET"})
     */
    public function show(Poption $poption): Response
    {
        return $this->render('poption/show.html.twig', [
            'poption' => $poption,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="poption_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Poption $poption): Response
    {
        $form = $this->createForm(PoptionType::class, $poption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('poption_index');
        }

        return $this->render('poption/edit.html.twig', [
            'poption' => $poption,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="poption_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Poption $poption): Response
    {
        if ($this->isCsrfTokenValid('delete' . $poption->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($poption);
            $entityManager->flush();
        }

        return $this->redirectToRoute('poption_index');
    }
}
