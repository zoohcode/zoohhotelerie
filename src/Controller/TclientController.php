<?php

namespace App\Controller;

use App\Entity\Tclient;
use App\Form\TclientType;
use App\Repository\TclientRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/tclient")
 */
class TclientController extends AbstractController
{
    /**
     * @Route("/", name="tclient_index", methods={"GET","POST"})
     */
    public function index(TclientRepository $tclientRepository, Request $request): Response
    {
        $tclient = new Tclient();
        $dataexp = $request->request->get('dateexp');
        $valider = $request->request->get('save');
        if (isset($valider)) {
            $tclient->setNom($request->request->get('nom'));
            $tclient->setPrenom($request->request->get('prenom'));
            $tclient->setTelephone($request->request->get('tel'));
            $tclient->setNumpasseport($request->request->get('numpiece'));
            if ($dataexp == null) {
                $tclient->setDatexp(null);
            } else {
                $tclient->setDatexp(new \DateTime($dataexp));
            }
    
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tclient);
            $entityManager->flush();
        }
            
        
        return $this->render('tclient/index.html.twig', [
            'client' =>  $tclient,
            'tclients' => $tclientRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tclient_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tclient = new Tclient();
        $form = $this->createForm(TclientType::class, $tclient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tclient);
            $entityManager->flush();

            return $this->redirectToRoute('tclient_index');
        }

        return $this->render('tclient/new.html.twig', [
            'tclient' => $tclient,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tclient_show", methods={"GET"})
     */
    public function show(Tclient $tclient): Response
    {
        return $this->render('tclient/show.html.twig', [
            'tclient' => $tclient,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tclient_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tclient $tclient): Response
    {
        $form = $this->createForm(TclientType::class, $tclient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tclient_index');
        }

        return $this->render('tclient/edit.html.twig', [
            'tclient' => $tclient,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tclient_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tclient $tclient): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tclient->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tclient);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tclient_index');
    }
}
