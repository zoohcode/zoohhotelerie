<?php

namespace App\Controller;

use App\Entity\Treservation;
use App\Form\TreservationType;
use App\Repository\TreservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reservation")
 */
class TreservationController extends AbstractController
{
    /**
     * @Route("/", name="treservation_index", methods={"GET"})
     */
    public function index(TreservationRepository $treservationRepository): Response
    {
        return $this->render('treservation/index.html.twig', [
            'treservations' => $treservationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="treservation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $treservation = new Treservation();
        $form = $this->createForm(TreservationType::class, $treservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($treservation);
            $entityManager->flush();

            return $this->redirectToRoute('treservation_index');
        }

        return $this->render('treservation/new.html.twig', [
            'treservation' => $treservation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="treservation_show", methods={"GET"})
     */
    public function show(Treservation $treservation): Response
    {
        return $this->render('treservation/show.html.twig', [
            'treservation' => $treservation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="treservation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Treservation $treservation): Response
    {
        $form = $this->createForm(TreservationType::class, $treservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('treservation_index');
        }

        return $this->render('treservation/edit.html.twig', [
            'treservation' => $treservation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="treservation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Treservation $treservation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$treservation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($treservation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('treservation_index');
    }
}
