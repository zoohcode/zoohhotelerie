<?php

namespace App\Controller;

use App\Entity\Tcategorie;
use App\Form\TcategorieType;
use App\Repository\TcategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/categorie")
 */
class TcategorieController extends AbstractController
{
    /**
     * @Route("/", name="tcategorie_index", methods={"GET","POST"})
     */
    public function index(Request $request, ValidatorInterface $validator, TcategorieRepository $tcategorieRepository): Response
    {
        $tcategorie = new Tcategorie();
        $errors = null;
        $save = $request->request->get('save');

        if ($save != null) {
            $tcategorie->setIntitule($request->request->get('intitule'));
            $errors = $validator->validate($tcategorie);
            $code = $tcategorieRepository->findOneBy([], ['id' => 'DESC'])->getId();
            $code++;
            $tcategorie->setCode('CAT' . substr(0, 3) . sprintf("%'.06d", $code));
            if (count($errors) == 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tcategorie);
                $entityManager->flush();
            }
        }

        return $this->render('tcategorie/index.html.twig', [
            'categories' => $tcategorieRepository->findBy(['isSupprimer' => false]),
            'errors' => $errors,
            'categorie' => $tcategorie
        ]);
    }



    /**
     * @Route("/{id}", name="tcategorie_show", methods={"GET"})
     */
    public function show(Tcategorie $tcategorie): Response
    {
        return $this->render('tcategorie/show.html.twig', [
            'tcategorie' => $tcategorie,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tcategorie_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tcategorie $tcategorie): Response
    {
        $save = $request->request->get('save');
        if (isset($save)) {
            $tcategorie->setIntitule($request->request->get('intitule2'));
            // dd($tcategorie);
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->redirectToRoute('tcategorie_index');
    }


    /**
     * @Route("/{id}", name="tcategorie_delete", methods={"DELETE"})
     */
    public function statut(Request $request, Tcategorie $tcategorie): Response
    {
        $tcategorie->setIsSupprimer(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        $this->addFlash('success', 'suppression éffectuée avec success!');
        return $this->redirectToRoute('tcategorie_index');
    }
}
