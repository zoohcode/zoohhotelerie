<?php

namespace App\Controller;

use App\Entity\Tchambre;
use App\Form\TchambreType;
use App\Repository\PoptionRepository;
use App\Repository\TchambreRepository;
use App\Repository\TcategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/chambre")
 */
class TchambreController extends AbstractController
{
    /**
     * @Route("/", name="tchambre_index", methods={"GET","POST"})
     */
    public function index(TchambreRepository $tchambreRepository, Request $request, TcategorieRepository $tcategorieRepository, PoptionRepository $poptionRepository): Response
    {
        $tchambre = new Tchambre();
        $save = $request->request->get('save');
        $categorie = $request->request->get('categorie');
        $option = $request->request->get('option');
        //dd($option);
        if ($save != null) {
            if ($option != null) {
                foreach ($option as  $value) {
                    //  dd($value);
                    $opt = $poptionRepository->find($value);
                    // dd($opt);
                    $tchambre->addPoption($opt);
                }
            }
            $tchambre->setNumchambre($request->request->get('num'));
            $tchambre->setPrix($request->request->get('prix'));
            $tchambre->setTcategories($tcategorieRepository->find($categorie));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tchambre);
            $entityManager->flush();
        }

        return $this->render('tchambre/index.html.twig', [
            'options' => $poptionRepository->findAll(),
            'categories' => $tcategorieRepository->findAll(),
            'tchambres' => $tchambreRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tchambre_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tchambre = new Tchambre();
        $form = $this->createForm(TchambreType::class, $tchambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tchambre);
            $entityManager->flush();

            return $this->redirectToRoute('tchambre_index');
        }

        return $this->render('tchambre/new.html.twig', [
            'tchambre' => $tchambre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tchambre_show", methods={"GET"})
     */
    public function show(Tchambre $tchambre): Response
    {
        return $this->render('tchambre/show.html.twig', [
            'tchambre' => $tchambre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tchambre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tchambre $tchambre): Response
    {
        $form = $this->createForm(TchambreType::class, $tchambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tchambre_index');
        }

        return $this->render('tchambre/edit.html.twig', [
            'tchambre' => $tchambre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tchambre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tchambre $tchambre): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tchambre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tchambre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tchambre_index');
    }
}
