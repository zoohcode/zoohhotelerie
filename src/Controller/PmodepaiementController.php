<?php

namespace App\Controller;

use App\Entity\Pmodepaiement;
use App\Form\PmodepaiementType;
use App\Repository\PmodepaiementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/modepaiement")
 */
class PmodepaiementController extends AbstractController
{
    /**
     * @Route("/", name="pmodepaiement_index", methods={"GET"})
     */
    public function index(PmodepaiementRepository $pmodepaiementRepository): Response
    {
        return $this->render('pmodepaiement/index.html.twig', [
            'pmodepaiements' => $pmodepaiementRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pmodepaiement_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pmodepaiement = new Pmodepaiement();
        $form = $this->createForm(PmodepaiementType::class, $pmodepaiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pmodepaiement);
            $entityManager->flush();

            return $this->redirectToRoute('pmodepaiement_index');
        }

        return $this->render('pmodepaiement/new.html.twig', [
            'pmodepaiement' => $pmodepaiement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pmodepaiement_show", methods={"GET"})
     */
    public function show(Pmodepaiement $pmodepaiement): Response
    {
        return $this->render('pmodepaiement/show.html.twig', [
            'pmodepaiement' => $pmodepaiement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pmodepaiement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pmodepaiement $pmodepaiement): Response
    {
        $form = $this->createForm(PmodepaiementType::class, $pmodepaiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pmodepaiement_index');
        }

        return $this->render('pmodepaiement/edit.html.twig', [
            'pmodepaiement' => $pmodepaiement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pmodepaiement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pmodepaiement $pmodepaiement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pmodepaiement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pmodepaiement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pmodepaiement_index');
    }
}
