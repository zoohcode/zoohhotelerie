<?php

namespace App\Controller;

use App\Entity\Occupant;
use App\Entity\Tchambre;
use App\Entity\Treservation;
use App\Repository\TclientRepository;
use App\Repository\OccupantRepository;
use App\Repository\TcategorieRepository;
use App\Repository\TchambreRepository;
use App\Repository\TreservationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/acceuil", name="home")
     */
    public function index(TcategorieRepository $tcategorieRepository, Request $request,TreservationRepository $treservationRepository, TchambreRepository $tchambreRepository, TclientRepository $tclientRepository): Response
    {
        //$chambres = $paginator->paginate($tchambreRepository->allchambres(), $request->query->getInt('page', 1), 12);
        $chambres = $tchambreRepository->findBy(['isSupprimer'=>false]);
         //affichage du full calendar 
        $reservations = [];
        $color = ['fc-event-solid-info fc-event-light', 'fc-event-solid-success fc-event-light', 'fc-event-solid-danger fc-event-light', 'fc-event-solid-primary fc-event-light'];     
        $listereservation = $treservationRepository->findBy(['statut'=>false, 'isConfirmer' => false]);
        foreach ($listereservation as $value) {
            $reservations[] = [
                //'title' => $value->getChambres()->getNumchambre(),
                'start' =>  date_format($value->getDatedebut(), 'Y-m-d'),
                'end' =>  date_format($value->getDatefin(), 'Y-m-d'),
                'description' => $value->getTclients()->getNom() . ' ' . $value->getTclients()->getPrenom(),
                'className' => $color[array_rand($color)] 
            ];
        }
        $reservations = json_encode($reservations, true);
       // dd($reservations);
        return $this->render('home/index.html.twig', [
            'chambresList' => $chambres,
            'categories' => $tcategorieRepository->findAll(),
            'clients' => $tclientRepository->findAll(),
            'reservations' => $reservations,
            'occuppes' => $treservationRepository->findBy(['isConfirmer'=>false, 'statut'=>false]),
            'chambres' => $tchambreRepository->findBy(['Isoccuper' => false])
        ]);
    }

    /**
     * @Route("/add/{id}", name="add_room", methods={"GET","POST"})
     */
    public function FunctionName(Tchambre $tchambre, Request $request, TclientRepository $tclientRepository)
    {
        $occuppant = new Occupant;
        $datedebut = $request->request->get('datedebut');
        $datefin = $request->request->get('datefin');
        $occuppant->setClients($tclientRepository->find($request->request->get('clients')));
        $occuppant->setChambres($tchambre);
        if ($datedebut == null && $datefin == null) {
            $occuppant->setDebut(null);
            $occuppant->setFin(null);
        } else {
            $occuppant->setDebut(new \DateTime($datedebut));
            $occuppant->setFin(new \DateTime($datefin));
        }
        $tchambre->setIsoccuper(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($occuppant);
        $entityManager->flush();
        // dd($occuppant);
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/addreserv/", name="add_reserver", methods={"GET","POST"})
     */
    public function addreserver(Request $request, TclientRepository $tclientRepository)
    {
        $reservation = new Treservation;

        $datedebut = $request->request->get('datedebut');
        $datefin = $request->request->get('datefin');
        $reservation->setTclients($tclientRepository->find($request->request->get('clients')));
        //$reservation->setChambres($tchambre);
        $reservation->setMontantarh($request->request->get('montant'));
        if ($datedebut == null && $datefin == null) {
            $reservation->setDatedebut(null);
            $reservation->setDatefin(null);
        } else {
            $reservation->setDatedebut(new \DateTime($datedebut));
            $reservation->setDatefin(new \DateTime($datefin));
        }
       // $tchambre->setIsReserver(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reservation);
        $entityManager->flush();
        // dd($occuppant);
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/free/{id}", name="isfree", methods={"GET","POST"})
     */
    public function statut(Tchambre $tchambre, Request $request): Response
    {
        $tchambre->setIsoccuper(false);
       // dd($tchambre);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/reserver/{id}", name="isReserver", methods={"GET","POST"})
     */
    public function statutisReserver(Treservation $reservation, Request $request,TchambreRepository $tchambreRepository): Response
    {   //dd($reservation);
        $occuppant = new Occupant;
        $occuppant->setChambres($tchambreRepository->find($request->request->get('chambres')));
        $occuppant->setClients($reservation->getTclients());
        $occuppant->setDebut($reservation->getDatedebut());
        $occuppant->setFin($reservation->getDatefin());
        $tchambre = $occuppant->getChambres();
        $tchambre->setIsoccuper(true);
        $reservation->setIsConfirmer(true);
       // dd($tchambre,$reservation);
        //dd($reservation);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reservation);
        $entityManager->persist($occuppant);
        $entityManager->persist($tchambre);
        $entityManager->flush();
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/annuler/{id}", name="isAnnuler", methods={"GET","POST"})
     */
    public function statutisannule(Treservation $reservation, Request $request): Response
    {
        //$tchambre = $reservation->getChambres();
        //$tchambre->setIsReserver(false);
        $reservation->setStatut(true);
       // dd($tchambre,$reservation);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reservation);
       // $entityManager->persist($tchambre);
        $entityManager->flush();
        return $this->redirectToRoute('home');
    }
}
