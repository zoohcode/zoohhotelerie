<?php

namespace App\Controller;

use App\Entity\Psociete;
use App\Form\PsocieteType;
use App\Repository\PsocieteRepository;
use App\Repository\PtypesocieteRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/psociete")
 */
class PsocieteController extends AbstractController
{
    /**
     * @Route("/", name="psociete_index", methods={"GET","POST"})
     */
    public function index(Request $request, PtypesocieteRepository $ptypesocieteRepository, PsocieteRepository $psocieteRepository): Response
    {
        $enregistrer = $request->request->get('enregistrer');
        $typesocietes = null;
        if (isset($enregistrer)) {
            $nom = $request->request->get('nom');
            $adresse = $request->request->get('adresse');
            $ifu = $request->request->get('ifu');
            $rccm = $request->request->get('rccm');
            $tel = $request->request->get('tel');
            $mcf = $request->request->get('mcf');
            $typesocietes = $request->request->get('typesocietes');

            if (sizeof($psocieteRepository->findAll()) == 0) {

                $psociete = new Psociete();
                $psociete->setNomsocite($nom);
                $psociete->setAdresse($adresse);
                $psociete->setIfu($ifu);
                $psociete->setTelephone($tel);
                $psociete->setMcf($mcf);
                $psociete->setRccm($rccm);
                $psociete->setPtypesocietes($ptypesocieteRepository->find($typesocietes));

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($psociete);
                $entityManager->flush();
            } else {

                $psociete = $psocieteRepository->findOneBy([]);
                $psociete->setNomsocite($nom);
                $psociete->setAdresse($adresse);
                $psociete->setIfu($ifu);
                $psociete->setTelephone($tel);
                $psociete->setMcf($mcf);
                $psociete->setRccm($rccm);
                $psociete->setPtypesocietes($ptypesocieteRepository->find($typesocietes));

                $this->getDoctrine()->getManager()->flush();
            }
        }


        return $this->render('psociete/index.html.twig', [
            'tp' => $typesocietes,
            'psociete' => $psocieteRepository->findOneBy([]),
            'typesocietes' => $ptypesocieteRepository->findAll(),

        ]);
    }

    /**
     * @Route("/new", name="psociete_new", methods={"GET","POST"})
     */
    public function new(Request $request, PtypesocieteRepository $ptypesocieteRepository, PsocieteRepository $psocieteRepository): Response
    {
        $psociete = new Psociete();
        $nom = $request->request->get('nom');
        $adresse = $request->request->get('adresse');
        $ifu = $request->request->get('ifu');
        $rccm = $request->request->get('rccm');
        $tel = $request->request->get('tel');
        $mcf = $request->request->get('mcf');
        // $email = $request->request->get('email');
        $typesocietes = $request->request->get('typesocietes');
        $enregistrer = $request->request->get('enregistrer');

        if (isset($enregistrer)) {
            $psociete->setNomsocite($nom);
            $psociete->setAdresse($adresse);
            $psociete->setIfu($ifu);
            $psociete->setTelephone($tel);
            $psociete->setMcf($mcf);
            $psociete->setRccm($rccm);
            $psociete->setPtypesocietes($ptypesocieteRepository->find($typesocietes));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($psociete);
            $entityManager->flush();

            return $this->redirectToRoute('psociete_new');
        }

        return $this->render('psociete/new.html.twig', [
            'psocietes' => $psocieteRepository->findAll(),
            'typesocietes' => $ptypesocieteRepository->findAll(),
            'tp' => $typesocietes
        ]);
    }

    /**
     * @Route("/{id}", name="psociete_show", methods={"GET"})
     */
    public function show(Psociete $psociete): Response
    {
        return $this->render('psociete/show.html.twig', [
            'psociete' => $psociete,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="psociete_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Psociete $psociete): Response
    {
        $form = $this->createForm(PsocieteType::class, $psociete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('psociete_index');
        }

        return $this->render('psociete/edit.html.twig', [
            'psociete' => $psociete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="psociete_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Psociete $psociete): Response
    {
        if ($this->isCsrfTokenValid('delete' . $psociete->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($psociete);
            $entityManager->flush();
        }

        return $this->redirectToRoute('psociete_index');
    }
}
