<?php

namespace App\Form;

use App\Entity\Psociete;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PsocieteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomsocite')
            ->add('adresse')
            ->add('ifu')
            ->add('telephone')
            ->add('mcf')
            ->add('rccm')
            ->add('ptypesocietes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Psociete::class,
        ]);
    }
}
