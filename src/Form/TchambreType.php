<?php

namespace App\Form;

use App\Entity\Poption;
use App\Entity\Tchambre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TchambreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix')
            ->add('description')
            ->add('numchambre')
            ->add('tcategories')
            ->add('poptions', EntityType::class, [
            'class' => Poption::class,
            'choice_label' => 'intitule',
            'multiple' => true,
            'required' => false
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tchambre::class,
        ]);
    }
}
