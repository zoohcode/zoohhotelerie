<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Poption;
use App\Entity\Tclient;
use App\Entity\Tchambre;
use App\Entity\Tcategorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $clients = array();
        for ($i = 0; $i < 8; $i++) {
            $clients[$i] = new Tclient();
            $clients[$i]->setNom($faker->lastName);
            $clients[$i]->setPrenom($faker->firstName);

            $manager->persist($clients[$i]);
        }

        // nouvelle boucle pour créer des categories

        $luxeCategory = new Tcategorie();
        $luxeCategory->setIntitule('Luxe');

        $ordinaireCategory = new Tcategorie();
        $ordinaireCategory->setIntitule('Ordinaire');

        $pregoCategory = new Tcategorie();
        $pregoCategory->setIntitule('Présidentielle');


        $manager->persist($luxeCategory);
        $manager->persist($ordinaireCategory);
        $manager->persist($pregoCategory);

        $manager->flush();

        $this->addReference('luxeCategory', $luxeCategory);
        $this->addReference('ordinaireCategory', $ordinaireCategory);
        $this->addReference('pregoCategory', $pregoCategory);


        $option0 = new Poption();
        $option0->setIntitule('Climatiseur');

        $option1 = new Poption();
        $option1->setIntitule('Jacouzi');

        $option2 = new Poption();
        $option2->setIntitule('Brasseur');

        $option3 = new Poption();
        $option3->setIntitule('Mini-bar');

        $manager->persist($option0);
        $manager->persist($option1);
        $manager->persist($option2);
        $manager->persist($option3);

        $manager->flush();

        $this->addReference('option0', $option0);
        $this->addReference('option1', $option1);
        $this->addReference('option2', $option2);
        $this->addReference('option3', $option3);

        // on crée 4 auteurs avec noms et prénoms "aléatoires" en français
        $chambre = array();
        for ($i = 0; $i <= 10; $i++) {
            $chambre[$i] = new Tchambre();
            $chambre[$i]->setNumchambre($i);
            $chambre[$i]->setPrix($faker->numberBetween(10000000, 20000000));
            $chambre[$i]->setTcategories($manager->merge($this->getReference('luxeCategory')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option0')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option1')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option2')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option3')));
         

            $manager->persist($chambre[$i]);
        }
        // on crée 4 auteurs avec noms et prénoms "aléatoires" en français
        $chambre = array();
        for ($i = 11; $i <= 20; $i++) {
            $chambre[$i] = new Tchambre();
            $chambre[$i]->setNumchambre($i);
            $chambre[$i]->setPrix($faker->numberBetween(10000000, 20000000));
            $chambre[$i]->setTcategories($manager->merge($this->getReference('ordinaireCategory')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option0')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option1')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option2')));
            $manager->persist($chambre[$i]);
        }
        // on crée 4 auteurs avec noms et prénoms "aléatoires" en français
        $chambre = array();
        for ($i = 21; $i <= 30; $i++) {
            $chambre[$i] = new Tchambre();
            $chambre[$i]->setNumchambre($i);
            $chambre[$i]->setPrix($faker->numberBetween(10000000, 20000000));
            $chambre[$i]->setTcategories($manager->merge($this->getReference('pregoCategory')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option0')));
            $chambre[$i]->addPoption($manager->merge($this->getReference('option1')));
            $manager->persist($chambre[$i]);
        }
        
        $manager->flush();
    }
}
