<?php

namespace App\Entity;

use App\Repository\OccupantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OccupantRepository::class)
 */
class Occupant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    

    /**
     * @ORM\ManyToOne(targetEntity=Tclient::class, inversedBy="occupants")
     */
    private $clients;

    /**
     * @ORM\ManyToOne(targetEntity=Tchambre::class, inversedBy="occupants")
     */
    private $chambres;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $debut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbrnuite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClients(): ?Tclient
    {
        return $this->clients;
    }

    public function setClients(?Tclient $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    public function getChambres(): ?Tchambre
    {
        return $this->chambres;
    }

    public function setChambres(?Tchambre $chambres): self
    {
        $this->chambres = $chambres;

        return $this;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(?\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(?\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getNbrnuite(): ?int
    {
        return $this->nbrnuite;
    }

    public function setNbrnuite(?int $nbrnuite): self
    {
        $this->nbrnuite = $nbrnuite;

        return $this;
    }
}
