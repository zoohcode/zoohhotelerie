<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numfacture;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datefacture;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $montantttc;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $montantht;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $montantnet;

    /**
     * @ORM\ManyToOne(targetEntity=Tclient::class, inversedBy="factures")
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity=Detailfacture::class, mappedBy="factures")
     */
    private $detailfactures;

    public function __construct()
    {
        $this->detailfactures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumfacture(): ?string
    {
        return $this->numfacture;
    }

    public function setNumfacture(?string $numfacture): self
    {
        $this->numfacture = $numfacture;

        return $this;
    }

    public function getDatefacture(): ?\DateTimeInterface
    {
        return $this->datefacture;
    }

    public function setDatefacture(?\DateTimeInterface $datefacture): self
    {
        $this->datefacture = $datefacture;

        return $this;
    }

    public function getMontantttc(): ?int
    {
        return $this->montantttc;
    }

    public function setMontantttc(?int $montantttc): self
    {
        $this->montantttc = $montantttc;

        return $this;
    }

    public function getMontantht(): ?int
    {
        return $this->montantht;
    }

    public function setMontantht(?int $montantht): self
    {
        $this->montantht = $montantht;

        return $this;
    }

    public function getMontantnet(): ?int
    {
        return $this->montantnet;
    }

    public function setMontantnet(?int $montantnet): self
    {
        $this->montantnet = $montantnet;

        return $this;
    }

    public function getClients(): ?Tclient
    {
        return $this->clients;
    }

    public function setClients(?Tclient $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * @return Collection|Detailfacture[]
     */
    public function getDetailfactures(): Collection
    {
        return $this->detailfactures;
    }

    public function addDetailfacture(Detailfacture $detailfacture): self
    {
        if (!$this->detailfactures->contains($detailfacture)) {
            $this->detailfactures[] = $detailfacture;
            $detailfacture->setFactures($this);
        }

        return $this;
    }

    public function removeDetailfacture(Detailfacture $detailfacture): self
    {
        if ($this->detailfactures->removeElement($detailfacture)) {
            // set the owning side to null (unless already changed)
            if ($detailfacture->getFactures() === $this) {
                $detailfacture->setFactures(null);
            }
        }

        return $this;
    }
}
