<?php

namespace App\Entity;

use App\Repository\PreductionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PreductionRepository::class)
 */
class Preduction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datereduct;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $montantreduuct;

    /**
     * @ORM\ManyToOne(targetEntity=Tchambre::class, inversedBy="preductions")
     */
    private $tchambres;

    /**
     * @ORM\ManyToOne(targetEntity=Tclient::class, inversedBy="preductions")
     */
    private $tclients;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatereduct(): ?\DateTimeInterface
    {
        return $this->datereduct;
    }

    public function setDatereduct(?\DateTimeInterface $datereduct): self
    {
        $this->datereduct = $datereduct;

        return $this;
    }

    public function getMontantreduuct(): ?int
    {
        return $this->montantreduuct;
    }

    public function setMontantreduuct(?int $montantreduuct): self
    {
        $this->montantreduuct = $montantreduuct;

        return $this;
    }

    public function getTchambres(): ?Tchambre
    {
        return $this->tchambres;
    }

    public function setTchambres(?Tchambre $tchambres): self
    {
        $this->tchambres = $tchambres;

        return $this;
    }

    public function getTclients(): ?Tclient
    {
        return $this->tclients;
    }

    public function setTclients(?Tclient $tclients): self
    {
        $this->tclients = $tclients;

        return $this;
    }
}
