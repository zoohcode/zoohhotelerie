<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TchambreRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=TchambreRepository::class)
 * @UniqueEntity("numchambre")
 */
class Tchambre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $numchambre;

    /**
     * @ORM\ManyToOne(targetEntity=Tcategorie::class, inversedBy="tchambres")
     */
    private $tcategories;

    /**
     * @ORM\ManyToMany(targetEntity=Poption::class, inversedBy="tchambres")
     */
    private $poptions;

    /**
     * @ORM\OneToMany(targetEntity=Preduction::class, mappedBy="tchambres")
     */
    private $preductions;

    /**
     * @ORM\OneToMany(targetEntity=Occupant::class, mappedBy="chambres")
     */
    private $occupants = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Isoccuper = false; 

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSupprimer = false;

    /**
     * @ORM\OneToMany(targetEntity=Treservation::class, mappedBy="chambres")
     */
    private $treservations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isReserver = false;



    public function __construct()
    {
        $this->poptions = new ArrayCollection();
        $this->preductions = new ArrayCollection();
        $this->occupants = new ArrayCollection();
        $this->treservations = new ArrayCollection();
      
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumchambre(): ?string
    {
        return $this->numchambre;
    }

    public function setNumchambre(?string $numchambre): self
    {
        $this->numchambre = $numchambre;

        return $this;
    }

    public function getTcategories(): ?Tcategorie
    {
        return $this->tcategories;
    }

    public function setTcategories(?Tcategorie $tcategories): self
    {
        $this->tcategories = $tcategories;

        return $this;
    }

    /**
     * @return Collection|Poption[]
     */
    public function getPoptions(): Collection
    {
        return $this->poptions;
    }

    public function addPoption(Poption $poption): self
    {
        if (!$this->poptions->contains($poption)) {
            $this->poptions[] = $poption;
        }

        return $this;
    }

    public function removePoption(Poption $poption): self
    {
        $this->poptions->removeElement($poption);

        return $this;
    }

    /**
     * @return Collection|Preduction[]
     */
    public function getPreductions(): Collection
    {
        return $this->preductions;
    }

    public function addPreduction(Preduction $preduction): self
    {
        if (!$this->preductions->contains($preduction)) {
            $this->preductions[] = $preduction;
            $preduction->setTchambres($this);
        }

        return $this;
    }

    public function removePreduction(Preduction $preduction): self
    {
        if ($this->preductions->removeElement($preduction)) {
            // set the owning side to null (unless already changed)
            if ($preduction->getTchambres() === $this) {
                $preduction->setTchambres(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Occupant[]
     */
    public function getOccupants()
    {

        if (sizeof($this->occupants->getValues()) > 0) {

            return max($this->occupants->getValues());
        } else {
            return null;
        }
    }

    public function addOccupant(Occupant $occupant): self
    {
        if (!$this->occupants->contains($occupant)) {
            $this->occupants[] = $occupant;
            $occupant->setChambres($this);
        }

        return $this;
    }

    public function removeOccupant(Occupant $occupant): self
    {
        if ($this->occupants->removeElement($occupant)) {
            // set the owning side to null (unless already changed)
            if ($occupant->getChambres() === $this) {
                $occupant->setChambres(null);
            }
        }

        return $this;
    }

    public function getIsoccuper(): ?bool
    {
        return $this->Isoccuper;
    }

    public function setIsoccuper(bool $Isoccuper): self
    {
        $this->Isoccuper = $Isoccuper;

        return $this;
    }

    public function getIsSupprimer(): ?bool
    {
        return $this->isSupprimer;
    }

    public function setIsSupprimer(bool $isSupprimer): self
    {
        $this->isSupprimer = $isSupprimer;

        return $this;
    }

    public function __toString()
    {
        return $this->numchambre;
    }

    /**
     * @return Collection|Treservation[]
     */
    public function getTreservations()
    {
        if (sizeof($this->treservations->getValues()) > 0) {

            return max($this->treservations->getValues());
        } else {
            return null;
        }
       //return $this->treservations->getValues();
    }

    public function addTreservation(Treservation $treservation): self
    {
        if (!$this->treservations->contains($treservation)) {
            $this->treservations[] = $treservation;
            $treservation->setChambres($this);
        }

        return $this;
    }

    public function removeTreservation(Treservation $treservation): self
    {
        if ($this->treservations->removeElement($treservation)) {
            // set the owning side to null (unless already changed)
            if ($treservation->getChambres() === $this) {
                $treservation->setChambres(null);
            }
        }

        return $this;
    }

    public function getIsReserver(): ?bool
    {
        return $this->isReserver;
    }

    public function setIsReserver(bool $isReserver): self
    {
        $this->isReserver = $isReserver;

        return $this;
    }
}
