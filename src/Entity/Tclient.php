<?php

namespace App\Entity;

use App\Repository\TclientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TclientRepository::class)
 */
class Tclient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numpasseport;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datexp;

    /**
     * @ORM\OneToMany(targetEntity=Treservation::class, mappedBy="tclients")
     */
    private $treservations;

    /**
     * @ORM\OneToMany(targetEntity=Preduction::class, mappedBy="tclients")
     */
    private $preductions;

    /**
     * @ORM\OneToMany(targetEntity=Occupant::class, mappedBy="clients")
     */
    private $occupants;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="clients")
     */
    private $factures;

    /**
     * @ORM\OneToMany(targetEntity=Paiement::class, mappedBy="clients")
     */
    private $paiements;

    /**
     * @ORM\OneToMany(targetEntity=Tprestation::class, mappedBy="clients")
     */
    private $tprestations;

    public function __construct()
    {
        $this->treservations = new ArrayCollection();
        $this->preductions = new ArrayCollection();
        $this->occupants = new ArrayCollection();
        $this->factures = new ArrayCollection();
        $this->paiements = new ArrayCollection();
        $this->tprestations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getNumpasseport(): ?string
    {
        return $this->numpasseport;
    }

    public function setNumpasseport(?string $numpasseport): self
    {
        $this->numpasseport = $numpasseport;

        return $this;
    }

    public function getDatexp(): ?\DateTimeInterface
    {
        return $this->datexp;
    }

    public function setDatexp(?\DateTimeInterface $datexp): self
    {
        $this->datexp = $datexp;

        return $this;
    }

    /**
     * @return Collection|Treservation[]
     */
    public function getTreservations(): Collection
    {
        return $this->treservations;
    }

    public function addTreservation(Treservation $treservation): self
    {
        if (!$this->treservations->contains($treservation)) {
            $this->treservations[] = $treservation;
            $treservation->setTclients($this);
        }

        return $this;
    }

    public function removeTreservation(Treservation $treservation): self
    {
        if ($this->treservations->removeElement($treservation)) {
            // set the owning side to null (unless already changed)
            if ($treservation->getTclients() === $this) {
                $treservation->setTclients(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Preduction[]
     */
    public function getPreductions(): Collection
    {
        return $this->preductions;
    }

    public function addPreduction(Preduction $preduction): self
    {
        if (!$this->preductions->contains($preduction)) {
            $this->preductions[] = $preduction;
            $preduction->setTclients($this);
        }

        return $this;
    }

    public function removePreduction(Preduction $preduction): self
    {
        if ($this->preductions->removeElement($preduction)) {
            // set the owning side to null (unless already changed)
            if ($preduction->getTclients() === $this) {
                $preduction->setTclients(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Occupant[]
     */
    public function getOccupants(): Collection
    {
        return $this->occupants;
    }

    public function addOccupant(Occupant $occupant): self
    {
        if (!$this->occupants->contains($occupant)) {
            $this->occupants[] = $occupant;
            $occupant->setClients($this);
        }

        return $this;
    }

    public function removeOccupant(Occupant $occupant): self
    {
        if ($this->occupants->removeElement($occupant)) {
            // set the owning side to null (unless already changed)
            if ($occupant->getClients() === $this) {
                $occupant->setClients(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom . ' ' . $this->prenom;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setClients($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->removeElement($facture)) {
            // set the owning side to null (unless already changed)
            if ($facture->getClients() === $this) {
                $facture->setClients(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Paiement[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setClients($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self
    {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getClients() === $this) {
                $paiement->setClients(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tprestation[]
     */
    public function getTprestations(): Collection
    {
        return $this->tprestations;
    }

    public function addTprestation(Tprestation $tprestation): self
    {
        if (!$this->tprestations->contains($tprestation)) {
            $this->tprestations[] = $tprestation;
            $tprestation->setClients($this);
        }

        return $this;
    }

    public function removeTprestation(Tprestation $tprestation): self
    {
        if ($this->tprestations->removeElement($tprestation)) {
            // set the owning side to null (unless already changed)
            if ($tprestation->getClients() === $this) {
                $tprestation->setClients(null);
            }
        }

        return $this;
    }
}
