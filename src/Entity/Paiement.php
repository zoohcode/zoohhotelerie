<?php

namespace App\Entity;

use App\Repository\PaiementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaiementRepository::class)
 */
class Paiement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datapaiement;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $montanttotal;

    /**
     * @ORM\ManyToOne(targetEntity=Pmodepaiement::class, inversedBy="paiements")
     */
    private $modepaiements;

    /**
     * @ORM\ManyToOne(targetEntity=Tclient::class, inversedBy="paiements")
     */
    private $clients;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatapaiement(): ?\DateTimeInterface
    {
        return $this->datapaiement;
    }

    public function setDatapaiement(?\DateTimeInterface $datapaiement): self
    {
        $this->datapaiement = $datapaiement;

        return $this;
    }

    public function getMontanttotal(): ?int
    {
        return $this->montanttotal;
    }

    public function setMontanttotal(?int $montanttotal): self
    {
        $this->montanttotal = $montanttotal;

        return $this;
    }

    public function getModepaiements(): ?Pmodepaiement
    {
        return $this->modepaiements;
    }

    public function setModepaiements(?Pmodepaiement $modepaiements): self
    {
        $this->modepaiements = $modepaiements;

        return $this;
    }

    public function getClients(): ?Tclient
    {
        return $this->clients;
    }

    public function setClients(?Tclient $clients): self
    {
        $this->clients = $clients;

        return $this;
    }
}
