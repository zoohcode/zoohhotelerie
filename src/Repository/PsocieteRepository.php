<?php

namespace App\Repository;

use App\Entity\Psociete;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Psociete|null find($id, $lockMode = null, $lockVersion = null)
 * @method Psociete|null findOneBy(array $criteria, array $orderBy = null)
 * @method Psociete[]    findAll()
 * @method Psociete[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsocieteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Psociete::class);
    }

    // /**
    //  * @return Psociete[] Returns an array of Psociete objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Psociete
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
