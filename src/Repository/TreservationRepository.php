<?php

namespace App\Repository;

use App\Entity\Treservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Treservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Treservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Treservation[]    findAll()
 * @method Treservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TreservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Treservation::class);
    }

    // /**
    //  * @return Treservation[] Returns an array of Treservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function getCategoriesAndJoinProducts()
    {
        return $this->createQueryBuilder("tr")
            ->leftJoin('tr', 'o', 'ON', 'o.post_id = p.id')
            ->addSelect("c") 
            ->getQuery()
            ->getResult();
    }
}
