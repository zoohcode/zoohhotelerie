<?php

namespace App\Repository;

use App\Entity\Preduction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Preduction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Preduction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Preduction[]    findAll()
 * @method Preduction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreductionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Preduction::class);
    }

    // /**
    //  * @return Preduction[] Returns an array of Preduction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Preduction
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
