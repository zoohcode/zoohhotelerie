<?php

namespace App\Repository;

use App\Entity\Pmodepaiement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pmodepaiement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pmodepaiement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pmodepaiement[]    findAll()
 * @method Pmodepaiement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PmodepaiementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pmodepaiement::class);
    }

    // /**
    //  * @return Pmodepaiement[] Returns an array of Pmodepaiement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pmodepaiement
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
