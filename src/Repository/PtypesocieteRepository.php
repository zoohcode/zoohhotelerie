<?php

namespace App\Repository;

use App\Entity\Ptypesociete;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ptypesociete|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ptypesociete|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ptypesociete[]    findAll()
 * @method Ptypesociete[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PtypesocieteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ptypesociete::class);
    }

    // /**
    //  * @return Ptypesociete[] Returns an array of Ptypesociete objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ptypesociete
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
