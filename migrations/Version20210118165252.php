<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210118165252 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE occupant ADD nbrnuite INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_155D7B82851B54C4 ON tchambre (numchambre)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE occupant DROP nbrnuite');
        $this->addSql('DROP INDEX UNIQ_155D7B82851B54C4 ON tchambre');
    }
}
