<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210104194813 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE treservation_tchambre');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE treservation_tchambre (treservation_id INT NOT NULL, tchambre_id INT NOT NULL, INDEX IDX_FC0DB9EE327A5208 (treservation_id), INDEX IDX_FC0DB9EE5DE187BF (tchambre_id), PRIMARY KEY(treservation_id, tchambre_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE treservation_tchambre ADD CONSTRAINT FK_FC0DB9EE327A5208 FOREIGN KEY (treservation_id) REFERENCES treservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE treservation_tchambre ADD CONSTRAINT FK_FC0DB9EE5DE187BF FOREIGN KEY (tchambre_id) REFERENCES tchambre (id) ON DELETE CASCADE');
    }
}
