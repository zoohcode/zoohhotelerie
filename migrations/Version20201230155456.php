<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201230155456 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE occupant (id INT AUTO_INCREMENT NOT NULL, clients_id INT DEFAULT NULL, chambres_id INT DEFAULT NULL, debut DATE DEFAULT NULL, fin DATE DEFAULT NULL, INDEX IDX_E7D9DBCAAB014612 (clients_id), INDEX IDX_E7D9DBCA8BEC3FB7 (chambres_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE occupant ADD CONSTRAINT FK_E7D9DBCAAB014612 FOREIGN KEY (clients_id) REFERENCES tclient (id)');
        $this->addSql('ALTER TABLE occupant ADD CONSTRAINT FK_E7D9DBCA8BEC3FB7 FOREIGN KEY (chambres_id) REFERENCES tchambre (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B35C8F0E77153098 ON tcategorie (code)');
        $this->addSql('ALTER TABLE tchambre ADD isoccuper TINYINT(1) NOT NULL, ADD is_supprimer TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE occupant');
        $this->addSql('DROP INDEX UNIQ_B35C8F0E77153098 ON tcategorie');
        $this->addSql('ALTER TABLE tchambre DROP isoccuper, DROP is_supprimer');
    }
}
