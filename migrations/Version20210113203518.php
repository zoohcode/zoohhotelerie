<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210113203518 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tprestation DROP FOREIGN KEY FK_973E7746AB014612');
        $this->addSql('DROP INDEX IDX_973E7746AB014612 ON tprestation');
        $this->addSql('ALTER TABLE tprestation DROP clients_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tprestation ADD clients_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tprestation ADD CONSTRAINT FK_973E7746AB014612 FOREIGN KEY (clients_id) REFERENCES tclient (id)');
        $this->addSql('CREATE INDEX IDX_973E7746AB014612 ON tprestation (clients_id)');
    }
}
