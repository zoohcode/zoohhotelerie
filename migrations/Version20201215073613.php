<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201215073613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pmodepaiement CHANGE statut statut TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE tprestation CHANGE isvendable isvendable TINYINT(1) NOT NULL, CHANGE isfree isfree TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE treservation CHANGE statut statut TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pmodepaiement CHANGE statut statut TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE tprestation CHANGE isvendable isvendable TINYINT(1) DEFAULT NULL, CHANGE isfree isfree TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE treservation CHANGE statut statut TINYINT(1) DEFAULT NULL');
    }
}
