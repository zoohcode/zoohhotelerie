<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210104195302 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE treservation ADD chambres_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE treservation ADD CONSTRAINT FK_75D6845D8BEC3FB7 FOREIGN KEY (chambres_id) REFERENCES tchambre (id)');
        $this->addSql('CREATE INDEX IDX_75D6845D8BEC3FB7 ON treservation (chambres_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE treservation DROP FOREIGN KEY FK_75D6845D8BEC3FB7');
        $this->addSql('DROP INDEX IDX_75D6845D8BEC3FB7 ON treservation');
        $this->addSql('ALTER TABLE treservation DROP chambres_id');
    }
}
